#-------------------------------------------------
#
# Project created by QtCreator 2018-05-19T15:15:53
#
#-------------------------------------------------


QT       -= gui

TARGET = tst_threadpool
CONFIG   += console
CONFIG   -= app_bundle
CONFIG += c++17

TEMPLATE = app

LIBS += -lgtest -lpcosynchro

SOURCES += \
    tst_threadpool.cpp

HEADERS += \
    threadpool.h

