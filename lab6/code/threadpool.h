#ifndef THREADPOOL_H
#define THREADPOOL_H

#include <cassert>
#include <iostream>
#include <queue>
#include <vector>

#include <pcosynchro/pcoconditionvariable.h>
#include <pcosynchro/pcologger.h>
#include <pcosynchro/pcomutex.h>
#include <pcosynchro/pcothread.h>

class Runnable {
public:
  /*
   * An empty virtual destructor
   */
  virtual ~Runnable() {}
  /*
   * Function executing the Runnable task.
   */
  virtual void run() = 0;

  /*
   * Function that can be called from the outside, to ask the cancellation
   * of the runnable.
   */
  virtual void cancelRun() = 0;

  /*
   * Simply retrieve an identifier for this runnable
   */
  virtual std::string id() = 0;
};

class ThreadPool {
private:
  std::queue<Runnable *> jobQueue;
  PcoMutex workAvailableLock;
  PcoConditionVariable workAvailableCond;
  std::vector<PcoThread *> runningThreads;
  size_t maxThreadNumber;
  size_t maxRunnablesWaiting;
  bool poolShouldStop;

  void maybeCreateThread() {
    // No need to protect the runningThreads array since this section is
    // protected by the monitor. A recursive mutex could be used, but we decided
    // not to since execution is linear.
    if (runningThreads.size() + 1 > maxThreadNumber)
      return;

    // Temporary. Needs to be improved to match "if a
    // thread is available and a job gets added but pool
    // is not full"
    runningThreads.push_back(new PcoThread([&]() {
      Runnable *runnable;
      while (!poolShouldStop) {
        workAvailableLock.lock();
        workAvailableCond.wait(&workAvailableLock);
        if (poolShouldStop)
          return workAvailableLock.unlock();
        runnable = jobQueue.front();
        jobQueue.pop();
        workAvailableLock.unlock();
        runnable->run();
      }
    }));
  }

  bool tryAddWork(Runnable *runnable) {
    if (jobQueue.size() + 1 > maxRunnablesWaiting)
      return false;
    jobQueue.push(runnable);
    return true;
  }

public:
  ThreadPool(int maxThreadCount, int maxNbWaiting) {
    maxThreadNumber = maxThreadCount;
    maxRunnablesWaiting = maxNbWaiting;
    poolShouldStop = false;
    logger().setVerbosity(1);
  }

  /// @brief Destructor simply discards any queued jobs.
  ~ThreadPool() {
    poolShouldStop = true;
    while (!jobQueue.empty())
      jobQueue.pop();
    workAvailableCond.notifyAll();
    for (size_t i = 0; i < runningThreads.size(); ++i)
      runningThreads[i]->join();
    for (size_t i = 0; i < runningThreads.size(); ++i)
      delete runningThreads[i];
  }

  /*
   * Start a runnable. If a thread in the pool is available, assign the
   * runnable to it. If no thread is available but the pool can grow, create a
   * new pool thread and assign the runnable to it. If no thread is available
   * and the pool is at max capacity and there are less than maxNbWaiting
   * threads waiting, block the caller until a thread becomes available again,
   * and else do not run the runnable. If the runnable has been started, returns
   * true, and else (the last case), return false.
   */
  bool start(Runnable *runnable) {
    // Enter monitor
    workAvailableLock.lock();
    // Add runnable to queue if able
    if (!tryAddWork(runnable)) {
      return false;
    }
    // Handle potential thread creation
    maybeCreateThread();
    // Get out of monitor
    workAvailableLock.unlock();
    // Signal one to let it rip!
    workAvailableCond.notifyOne();
    return true;
  }
};

#endif // THREADPOOL_H
