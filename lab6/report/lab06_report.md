---
toc: yes
date: 31 May 2021
title: Thread pool
subtitle: PCO, Lab 06
author:
- Joan Maillard
- Roosembert Palacios
---

# Introduction {.nopagebreak}

The premise of this lab work is to allow us to get hands-on experience with
mesa monitors, as well as thread pools.

# Design decisions

After analysis of the problem, in order to implement the thread poool in
accordance with the functional constraints list provided in the lab description
file, we needed to make use of a Mesa monitor in order to synchronize the
acquisition of Runnable-type objects by the worker threads handled by the thread
pool.

The method start() implements this monitor and when a thread executes, it enters
said monitor in order to acquire a runnable that exists in a job queue.

# Testing notes

Our code has an issue: tests don't pass, and we have been unable to debug our program.
We made use of the handout-provided testing functions. None were added, as we deemed
our understanding of the handout satisfactory in order to abide by the rules it sets
without attempting any further investigation. It is however important to us to note
that the unstable implementation of the tests are very much OS- and hardware-specific,
because it is based on time, which in turn assumes relative non-interference from
external preemption at the process level, as well as a consistent implementation
of threading libraries, and a consistent processing time. Therefore, another method
of testing would be preferable.

# Technical notes

Due to the requirement made towards the versatility of the implementation of the
generation of new threads on an as-needed basis, as well as the operational
complexity of adding Runnable objects to the thread pool, these two procedures
are handled by separated methods. These are however declared in the private
section of the ThreadPool class, which means their usage is restricted to our
own implementation. This also means that when called properly, such as they are
by the start() method, they do not require any additional protection, because they
are effectively just snippets of code that would impair readability should they
be implemented directly inside of start().

# Conclusion

As previously mentioned, our code doesn't work. We have tracked it down to an
exception arising on line 61 while the PcoConditionVariable waits. We have not
been able to find its source.

A possible extension of this assignment would be to increase the number of threads
to that of, for example, the available logical threads the hardware provides.
