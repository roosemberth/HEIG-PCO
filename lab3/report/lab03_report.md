---
toc: yes
date: 30 Mar 2021
title: Introduction to POSIX threads
subtitle: PCO, Lab 03
author:
- Joan Maillard
- Roosembert Palacios
---

# Introduction {.nopagebreak}

The premise of this lab work is to allow us to make use of one of the main concepts 
of this class: using threads, through the _PCOSynchro_ library, as well as _mutex_ and 
_semaphores_.

# Design decisions

After analysis of the problem, in order to implement the simulator in 
accordance with the functional constraints list provided in the lab description file, 
three types of critical sections were needed:

- Money handling
- Card status handling
- Handset status handling

Those three types allow us to keep complete control of data races, and prevent them from causing any problems, therefore making the whole program robust.

Our implementation allows the writing of the phone number digit by digit.

In order to handle point 4 regarding the number of digits in a phone number, our 
implementation locks the addition of more than 10 digits while the conversation is 
ongoing, which starts as soon as the 10th digit is entered. 
Given that it allows the writing of the phone number digit by digit, writing less 
than 10 digits doesn't trigger the start of a conversation. Writing more than 10 
digits starts a conversation by only taking into account the first 10.
Since the digit buffer is wiped after every conversation or hanging up of the 
handset, this doesn't cause any issues. 

In order to handle point 7 regarding the removal of the phone card, our 
implementation makes use of a mechanism that presumes that the absence of a card 
equates to having no credit. This means that removing the card automatically sets 
the active money counter to 0, hereby immediately terminating the conversation. The 
amount of money that was stored in the card before its removal is stored in a 
separate attribute, allowing for its removal and reinsertion without credit loss.

## Implementation decisions regarding undefined behaviors

After discussion with the assistant regarding the perceived lack of clarity of point 
8 of the functional constraints list relatively to point 2 thereof, it has been 
decided that either of the two following possibilities would be accepted:

- Allowing the removal and reinsertion of a phone card between handset pickup and 
call start
- Disallowing such operation

Our version of the code allows this operation.

# Testing notes

Given the nature of our application, the only reasonable way of testing it was to check that it would have the expected 
behavior when asked to perform its tasks. Therefore, based on the key points and the implementation of the program, we 
established a short and concise test protocol, expecting the correct results, as follows:

Test manipulation                           Expected behavior
------------------------------------------- ------------------------------------------------------
Run the program                             The program starts
Insert card, pick up, add coin, compose     A call start and ends when the credit is out
Insert card, pick up, compose               The call immediately says the user is out of credit
Insert card, compose                        No number is entered
Insert card, compose, pick up               The buffered numbers are entered into the composition
Pick up, compose                            The phone tells the user it needs a card before pickup
Pick up, insert card, compose               Idem
Insert card, pick up, remove card, compose  The call immediately says the user is out of credit
Insert, pick up, remove, insert, compose    The call starts according to the user's credit
Press q at any one step of the previous     The program stops without any further message
Insert, pick up, add coin, compose, hang up The call proceeds as expected and stops upon hang-up.
(after previous) compose again              The call starts according to the user's credit

All those test procedures proved conclusively functional.
# Technical notes

Due to the nature of the safety measures taken in order to handle critical sections 
in our work, guaranteeing the safe exiting of threads at any moment forces us to 
make use of an unspecified in common definitions of a mutex.

Specifically, in order to make sure that all threads would terminate correctly
at any point, we had to make use of a final leave() call to all CriticalSection
objects, regardless of whether they had been locked previously or not.

In our experience, _PCOSynchro_ mutexes are idempotent when releasing the lock,
so this is a non-issue. A more careful implementation would check whether the
mutex is locked before releasing it, but such functionality is not provided by
the library and was thus deemed out of the scope of the current lab.

# Conclusion

The provided implementation is very simple and limited and some of the
implemented behaviours could be surprising for a user.
For instance we disregard dialing during a conversation, whereas common practice
in [PSTN][pstn] is to rather send [DTMF][dtmf] codes through the voice link
which may be useful for automated processing of a user request.
Another possible extension would be to add some sort of signaling to the user
that their credit is about to run out; this could be combined with a button to
hold credit (during or off a call) so that the user can switch phone cards on
the fly.

[pstn]: https://en.wikipedia.org/wiki/Public_switched_telephone_network
[dtmf]: https://en.wikipedia.org/wiki/Dual-tone_multi-frequency_signaling
