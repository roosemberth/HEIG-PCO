#include <iostream>
#include <pcosynchro/pcothread.h>

#include "phone.h"
#include "phonemanager.h"

int main() {
  std::cout << "Simulateur de telephone\n"
               " I/O : Insertion/Retrait de la carte\n"
               " D/R : Decroche/Raccroche le combine\n"
               " chiffre [1..0]: Pave numerique de l'appareil\n"
               " q : Quitte l'application\n"
               " w, e, r, t, z, u, i, o, p : Piece de monnaie avec une valeur "
               "allant de 1 a 9\n\n";

  Phone phone;
  if (phone.initialize()) {
    PhoneManager manager(phone);
    PcoThread ThreadMonnaie(&PhoneManager::moneyTask, &manager);
    PcoThread ThreadCarte(&PhoneManager::cardTask, &manager);
    PcoThread ThreadCombine(&PhoneManager::handsetTask, &manager);
    PcoThread ThreadTouche(&PhoneManager::numberKeyTask, &manager);
    ThreadMonnaie.join();
    ThreadCarte.join();
    ThreadCombine.join();
    ThreadTouche.join();
  }
  return EXIT_FAILURE;
} /* fin de main */
