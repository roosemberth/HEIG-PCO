#ifndef PHONEINTERFACE_H
#define PHONEINTERFACE_H

/** Type des différentes pieces de monnaie, une valeur dans 1..9. */
typedef int PIECE;

class PhoneInterface {
public:
  /** Réalise toutes les initialisations nécessaire. Cette fonction
   * doit être appelée avant d'utiliser l'une des fonctions de cet interface.
   * \return 1 si tout est en ordre et 0 en cas d'erreur.
   */
  virtual int initialize() = 0;

  /** Fonction bloquante attendant qu'une carte soit insérée.
   *  Cette fonction est bloquante, attention à la traiter correctement. Elle
   *  attend que la touche 'I', en majuscule, soit pressée. Elle retourne
   * ensuite le montant disponible sur la carte. La première fois que la carte
   * est insérée, le montant est nul. Ensuite il est mémorisé de manière
   * interne. \return Le montant disponible sur la carte.
   */
  virtual int cardInserted() = 0;

  /** Fonction bloquante attendant que la carte soit récupérée.
   *  Cette fonction est bloquante, attention à la traiter correctement. Elle
   *  attend que la touche 'O', en majuscule, soit pressée.
   */
  virtual void cardCollected() = 0;

  /** Fonction permettant de modifier le montant de la carte.
   *  Le montant est mémorisé sur la carte, notamment si elle est ensuite
   *  retirée de la machine.
   *  \param montant Le nouveau montant de la carte.
   */
  virtual void setAmount(unsigned amount) = 0;

  /** Fonction bloquante attendant que le combiné soit décroché.
   *  Cette fonction est bloquante, attention à la traiter correctement. Elle
   *  attend que la touche 'D', en majuscule, soit pressée.
   */
  virtual void pickUp() = 0;

  /** Fonction bloquante attendant que le combiné soit raccroché.
   *  Cette fonction est bloquante, attention à la traiter correctement. Elle
   *  attend que la touche 'R', en majuscule, soit pressée.
   */
  virtual void hangUp() = 0;

  /** Fonction bloquante attendant qu'une touche de numérotation soit pressée.
   *  Cette fonction est bloquante, attention à la traiter correctement. Elle
   *  attend qu'une des touches du clavier entre '0' et '9' soit pressée,
   *  et retourne alors le nombre correspondant.
   *	\return un nombre de 0 à 9.
   */
  virtual int getKeyNumber() = 0;

  /** Fonction bloquante attendant qu'une pièce de monnaie soit introduite.
   *  Cette fonction est bloquante, attention à la traiter correctement. Elle
   *  attend qu'une des touches du clavier 'w', 'e', 'r', 't', 'z', 'u',
   *  'i', 'o' ou 'p'.
   *  La valeur de la pièce est alors retournée. La valeur est la suivante:
   *		- w: 1
   *		- e: 2
   *		- r: 3
   *		- t: 4
   *		- z: 5
   *		- u: 6
   *		- i: 7
   *		- o: 8
   *		- 9: 9
   * \return une piece de type PIECE, valeur entière entre 1 et 9.
   */
  virtual PIECE getPiece() = 0;

  /** Fonction wqui retoure vrai lorsque le programme est terminé.
   *  Permet aux différents threads de savoir si ils doivent s'arrêter.
   *  \return vrai lorsque le programme est terminé
   */
  virtual bool shouldQuit() = 0;
};

#endif // PHONEINTERFACE_H
