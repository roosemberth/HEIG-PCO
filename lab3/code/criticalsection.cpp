#include "criticalsection.h"

void CriticalSection::enter() { m_mutex.lock(); }

void CriticalSection::leave() { m_mutex.unlock(); }
