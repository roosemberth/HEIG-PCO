// Authors : Roosembert Palacios - Joan Maillard

#ifndef PHONEMANAGER_H
#define PHONEMANAGER_H

#define DURATION std::chrono::duration_cast<std::chrono::milliseconds>
#define TICK_DURATION 2000

#include <iostream>

#include <pcosynchro/pcothread.h>
#include <chrono>

#include "criticalsection.h"
#include "logging.h"
#include "phone.h"

class PhoneManager {
public:
  typedef std::chrono::high_resolution_clock HRClock;
  typedef std::chrono::milliseconds Interval;

  PhoneManager(Phone &phone) : phone(phone), intervalStart(HRClock::now()), creditTick(Interval(TICK_DURATION)) {}

  /* Tâche gérant le combiné */
  void handsetTask() {
    while (1) {
      phone.pickUp();
      if (phone.shouldQuit()) {
        leaveCriticalSections();
        return;
      }
      criticalHandset.enter();
        criticalCard.enter();
          if (cardInserted) {
            cardWasInsertedOnPickup = true;
          }
        criticalCard.leave();
        handSetPickedUp = true;
        Logging << "Combine en main \n";
      criticalHandset.leave();
      phone.hangUp();
      if (phone.shouldQuit()) {
        leaveCriticalSections();
        return;
      }
      criticalHandset.enter();
        cardWasInsertedOnPickup = false;
        handSetPickedUp = false;
        Logging << "Combine repose \n";
      criticalHandset.leave();
    }
  }

  /* Tâche gérant le pavé numérique (0-9) */
  void numberKeyTask() {
    bool isTalking = false;
    int touche;
    int numTouches = 0;
    int tempSum;
    bool tempCardStatus;
      while (1) {
        criticalHandset.enter();
        if (handSetPickedUp) { // empeche le tapage de numeros si le combine n'est pas decroche
          criticalHandset.leave();
          if (!isTalking) { // empeche le tapage de numeros si une conversation est en cours
            touche = phone.getKeyNumber();
            if (phone.shouldQuit()) {
              leaveCriticalSections();
              return;
            }
            ++numTouches;
            Logging << "Touche appuyée: " << touche << std::endl;
          } else if (phone.shouldQuit()) {
            leaveCriticalSections();
            return;
          }
          if (numTouches == 10) { // si le numero est de la bonne longueur
            criticalMoney.enter();
            tempSum = sum;
            criticalMoney.leave();
            if (tempSum > 0) { // si le credit est suffisant pour démarrer une conversation
              criticalCard.enter();
                tempCardStatus = cardWasInsertedOnPickup;
              criticalCard.leave();
                if (tempCardStatus) { // si la carte etait inseree au moment du decrochage du combine
                  isTalking = true;
                } else if (!isTalking) {
                  Logging << "Aucune conversation possible: la carte n'était pas insérée au moment du décrochage du combiné. \n";
                  numTouches = 0;
                }
            } else {
              Logging << "Pas de crédit ou aucune carte insérée. Fin de conversation. \n";
              resetInt(numTouches);
              isTalking = false;
            }
          } else {
            if (isTalking) {
                Logging << "Fin de conversation. \n";
              }
              isTalking = false;
          }
          if (isTalking) {
            talk();
          }
        } else {
          if (phone.shouldQuit()) {
            leaveCriticalSections();
            return;
          }
          criticalHandset.leave();
          resetInt(numTouches);
          if (isTalking) {
            Logging << "Fin de conversation. \n";
          }
          isTalking = false;
        }
    }
  }

  /* Tâche gérant l'arrivée des differentes pieces de monnaie. */
  void moneyTask() {
    bool tempCardStatus;
    PIECE piece; // piece a traiter
    while (1) {
      piece = phone.getPiece(); // lecteur d'une piece
      if (phone.shouldQuit()) {
        leaveCriticalSections();
        return;
      }
      Logging << "Monnaie entree: " << piece << "\n";
      criticalCard.enter();
        tempCardStatus = cardInserted;
      criticalCard.leave();
      if(tempCardStatus) {
        criticalMoney.enter(); // Adjonction de la valeur de la pièce au crédit
          sum += piece;
        criticalMoney.leave();
      } else {
        Logging << "Monnaie rendue: " << piece << "\n";
      }
    }
  }

  /* Tache gerant l'insertion et ejection de la carte de debit. */
  void cardTask() {
    while (1) {
      sum = phone.cardInserted();
      criticalCard.enter();
        cardInserted = true;
        criticalMoney.enter();
          sum = previousSum;
        criticalMoney.leave();
      criticalCard.leave();
      if (phone.shouldQuit()) {
        leaveCriticalSections();
        return;
      }
      Logging << "Montant disponible: " << sum << std::endl;
      phone.cardCollected();
      if (phone.shouldQuit()) {
        leaveCriticalSections();
        return;
      }
      criticalCard.enter();
        cardInserted = false;
      criticalCard.leave();
      criticalMoney.enter();
        /* Etant donne qu'aucun mecanisme n'existe pour sauvegarder le credit d'une carte dans la carte elle-meme
        l'etat de la somme restant est sauvegarde dans une variable separee. Cela permet la simplification d'un autre
        mecanisme qui est celui de la detection de l'ejection d'une carte lors d'une conversation.*/
        previousSum = sum;
        sum = 0; // fait tomber la somme a 0 si on retire la carte, interrompant du meme coup une eventuelle conversation
      criticalMoney.leave();
    }
  }

private:
  int sum{0};
  bool handSetPickedUp{false};
  Phone &phone;

  CriticalSection criticalMoney{};
  CriticalSection criticalHandset{};
  CriticalSection criticalCard{};
  bool cardInserted{false};
  bool cardWasInsertedOnPickup{false};
  int previousSum{0};
  Interval creditTick;
  std::chrono::_V2::system_clock::time_point intervalStart;

  void resetInt(int& num) {
    num = 0;
  }

  void talk() {
    if (DURATION(HRClock::now() - intervalStart).count() > creditTick.count()) { // toutes les 2 secondes, sans bloquer
      intervalStart = HRClock::now();
      Logging << "blabla...\n";
      criticalMoney.enter(); // decremente et reagit en fonction de sum
        --sum;
      criticalMoney.leave();
    }
  }

  void leaveCriticalSections() {
    criticalMoney.leave();
    criticalHandset.leave();
    criticalCard.leave();
  }
};

#endif // PHONEMANAGER_H
