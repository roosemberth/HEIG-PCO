#ifndef CRITICALSECTION_H
#define CRITICALSECTION_H

#include <pcosynchro/pcomutex.h>

class CriticalSection {
public:
  /** Procédure définissant l'entrée dans la section critique.
   *  Une section critique est fournie par l'interface, et peut être utilisée
   *  pour protéger les variables qui doivent l'être.
   *  Cette procédure est bloquante, le thread restant bloqué jusqu'à ce que la
   *  section critique soit relâchée par un autre.
   */
  void enter();

  /** Procédure définissant la sortie de la section critique.
   *  Une section critique est fournie par l'interface, et peut être utilisée
   *  pour protéger les variables qui doivent l'être.
   *  Si un autre thread est en attente d'accès à la section critique, il est
   *  relâché par cette procédure.
   *  Cette procédure ne doit être appelée qu'après avoir appelé
   *  Telephone_DebutSectionCritique().
   */
  void leave();

private:
  PcoMutex m_mutex;
};

#endif // CRITICALSECTION_H
