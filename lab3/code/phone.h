/** \mainpage phone
 * Cette documentation décrit l'interface fournie par le fichier phone.h.
 */

/** \file phone.h
 *  \author YTA
 *  \date 05.02.2021
 * \version 1.0.0
 * \section Description
 * Ce fichier définit l'interface permettant de gérer un téléphone.
 * Plusieurs des fonctions sont bloquantes. Il est dès lors nécessaire de
 * gérer le téléphone via plusieurs tâches, étant donné que l'utilisateur
 * peut effectuer les opérations dans un ordre quelconque.
 */

#ifndef PHONE_H
#define PHONE_H

#include <pcosynchro/pcoconditionvariable.h>
#include <pcosynchro/pcomutex.h>
#include <pcosynchro/pcosemaphore.h>
#include <pcosynchro/pcothread.h>

#include "phoneinterface.h"

class Phone : public PhoneInterface {
public:
  Phone();
  virtual ~Phone();

  /** Réalise toutes les initialisations nécessaire. Cette fonction
   * doit être appelée avant d'utiliser l'une des fonctions de cet interface.
   * \return 1 si tout est en ordre et 0 en cas d'erreur.
   */
  int initialize() override;

  /** Fonction bloquante attendant qu'une carte soit insérée.
   *  Cette fonction est bloquante, attention à la traiter correctement. Elle
   *  attend que la touche 'I', en majuscule, soit pressée. Elle retourne
   * ensuite le montant disponible sur la carte. La première fois que la carte
   * est insérée, le montant est nul. Ensuite il est mémorisé de manière
   * interne. \return Le montant disponible sur la carte.
   */
  int cardInserted() override;

  /** Fonction bloquante attendant que la carte soit récupérée.
   *  Cette fonction est bloquante, attention à la traiter correctement. Elle
   *  attend que la touche 'O', en majuscule, soit pressée.
   */
  void cardCollected() override;

  /** Fonction permettant de modifier le montant de la carte.
   *  Le montant est mémorisé sur la carte, notamment si elle est ensuite
   *  retirée de la machine.
   *  \param montant Le nouveau montant de la carte.
   */
  void setAmount(unsigned amount) override;

  /** Fonction bloquante attendant que le combiné soit décroché.
   *  Cette fonction est bloquante, attention à la traiter correctement. Elle
   *  attend que la touche 'D', en majuscule, soit pressée.
   */
  void pickUp() override;

  /** Fonction bloquante attendant que le combiné soit raccroché.
   *  Cette fonction est bloquante, attention à la traiter correctement. Elle
   *  attend que la touche 'R', en majuscule, soit pressée.
   */
  void hangUp() override;

  /** Fonction bloquante attendant qu'une touche de numérotation soit pressée.
   *  Cette fonction est bloquante, attention à la traiter correctement. Elle
   *  attend qu'une des touches du clavier entre '0' et '9' soit pressée,
   *  et retourne alors le nombre correspondant.
   *	\return un nombre de 0 à 9.
   */
  int getKeyNumber() override;

  /** Fonction bloquante attendant qu'une pièce de monnaie soit introduite.
   *  Cette fonction est bloquante, attention à la traiter correctement. Elle
   *  attend qu'une des touches du clavier 'w', 'e', 'r', 't', 'z', 'u',
   *  'i', 'o' ou 'p'.
   *  La valeur de la pièce est alors retournée. La valeur est la suivante:
   *		- w: 1
   *		- e: 2
   *		- r: 3
   *		- t: 4
   *		- z: 5
   *		- u: 6
   *		- i: 7
   *		- o: 8
   *		- 9: 9
   * \return une piece de type PIECE, valeur entière entre 1 et 9.
   */
  PIECE getPiece() override;

  bool shouldQuit() override;

protected:
  void quit();

  void insertCardAction();

  void cardCollectionAction();

  void processKey(char car);

#define BUFFERSIZE 4
  typedef struct Buffer {
    PcoMutex mutex;
    PcoConditionVariable waitNotFull, waitNotEmpty;
    int writeIndex, readIndex, nbElements;
    int content[BUFFERSIZE];
  } Buffer;

  Buffer bufferPiecesIntroduction;
  Buffer bufferKeysIntroduction;

  int initializeBuffer(Buffer *buffer);
  void freeBuffer(Buffer *buffer);
  int bufferGet(Buffer *buffer);
  int bufferPut(Buffer *buffer, int element);

  void handUpAction();
  void pickUpAction();

  void keysTask();

  std::unique_ptr<PcoThread> threadButton;

  int m_cardStatus{0};
  int m_handsetStatus{0};

  int m_cardAmount{0};
  int amountNextUse{0};

  PcoSemaphore m_semCardIn;
  PcoSemaphore m_semCardOut;
  PcoSemaphore m_semPickUp;
  PcoSemaphore m_semHangUp;

  bool m_shouldQuit{false};
};

#endif // PHONE_H
