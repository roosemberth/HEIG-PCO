#include <pcosynchro/pcothread.h>

#include "phone.h"

Phone::Phone() {
  if (initializeBuffer(&bufferKeysIntroduction)) {
    if (initializeBuffer(&bufferPiecesIntroduction)) {
      return;
    }
  }
}

Phone::~Phone() {
  if (threadButton.get() != nullptr) {
    threadButton->join();
  }
}

int Phone::initializeBuffer(Buffer *buffer) {
  buffer->writeIndex = buffer->readIndex = buffer->nbElements = 0;
  return 1;
}

void Phone::freeBuffer(Phone::Buffer * /*buffer*/) {}

int Phone::bufferGet(Phone::Buffer *buffer) {
  int tmp;
  buffer->mutex.lock();
  if (buffer->nbElements == 0) {
    buffer->waitNotEmpty.wait(&buffer->mutex);
  }
  tmp = buffer->content[buffer->readIndex];
  buffer->readIndex = (buffer->readIndex + 1) % BUFFERSIZE;
  buffer->nbElements -= 1;
  buffer->waitNotFull.notifyOne();
  buffer->mutex.unlock();
  return tmp;
}

int Phone::bufferPut(Phone::Buffer *buffer, int element) {
  buffer->mutex.lock();
  if (buffer->nbElements == BUFFERSIZE) {
    buffer->waitNotFull.waitForSeconds(&buffer->mutex, 1);
  }
  if (buffer->nbElements < BUFFERSIZE) {
    buffer->content[buffer->writeIndex] = element;
    buffer->nbElements += 1;
    buffer->waitNotEmpty.notifyOne();
    buffer->writeIndex = (buffer->writeIndex + 1) % BUFFERSIZE;
  } else
    element = 0;
  buffer->mutex.unlock();
  return element;
}

PIECE Phone::getPiece() { return (PIECE)bufferGet(&bufferPiecesIntroduction); }

int Phone::getKeyNumber() { return bufferGet(&bufferKeysIntroduction); }

int Phone::cardInserted() {
  m_semCardIn.acquire();
  return m_cardAmount;
}

void Phone::cardCollected() { m_semCardOut.acquire(); }

void Phone::insertCardAction() {
  if (m_cardStatus == 0) {
    m_cardAmount = amountNextUse;
    m_semCardIn.release();
    m_cardStatus = 1;
  }
}

void Phone::cardCollectionAction() {
  if (m_cardStatus == 1) {
    amountNextUse = m_cardAmount;
    m_cardAmount = 0;
    m_semCardOut.release();
    m_cardStatus = 0;
  }
}

void Phone::setAmount(unsigned amount) { m_cardAmount = amount; }

void Phone::pickUp() { m_semPickUp.acquire(); }

void Phone::hangUp() { m_semHangUp.acquire(); }

void Phone::pickUpAction() {
  if (m_handsetStatus == 0) {
    m_semPickUp.release();
    m_handsetStatus = 1;
  }
}

void Phone::handUpAction() {
  if (m_handsetStatus == 1) {
    m_semHangUp.release();
    m_handsetStatus = 0;
  }
}

bool Phone::shouldQuit() { return m_shouldQuit; }

void Phone::quit() {
  m_semCardIn.release();
  m_semCardOut.release();
  m_semPickUp.release();
  m_semHangUp.release();
  bufferPut(&bufferPiecesIntroduction, 0);
  bufferPut(&bufferKeysIntroduction, 0);
  m_shouldQuit = true;
}

void Phone::processKey(char car) {
  switch (car) {
  case 'D':
    pickUpAction();
    break;
  case 'R':
    handUpAction();
    break;
  case 'O':
    cardCollectionAction();
    break;
  case 'I':
    insertCardAction();
    break;
  case '1':
  case '2':
  case '3':
  case '4':
  case '5':
  case '6':
  case '7':
  case '8':
  case '9':
  case '0':
    bufferPut(&bufferKeysIntroduction, car - '0');
    break;
  case 'q':
    this->quit();
    return;
    break;
  case 'w':
    bufferPut(&bufferPiecesIntroduction, 1);
    break;
  case 'e':
    bufferPut(&bufferPiecesIntroduction, 2);
    break;
  case 'r':
    bufferPut(&bufferPiecesIntroduction, 3);
    break;
  case 't':
    bufferPut(&bufferPiecesIntroduction, 4);
    break;
  case 'z':
  case 'y':
    bufferPut(&bufferPiecesIntroduction, 5);
    break;
  case 'u':
    bufferPut(&bufferPiecesIntroduction, 6);
    break;
  case 'i':
    bufferPut(&bufferPiecesIntroduction, 7);
    break;
  case 'o':
    bufferPut(&bufferPiecesIntroduction, 8);
    break;
  case 'p':
    bufferPut(&bufferPiecesIntroduction, 9);
    break;
  default:
    break;
  }
}

void Phone::keysTask() {
  while (!m_shouldQuit) {
    processKey(getchar());
  }
}

int Phone::initialize() {
  threadButton = std::make_unique<PcoThread>(&Phone::keysTask, this);
  return 1;
}
