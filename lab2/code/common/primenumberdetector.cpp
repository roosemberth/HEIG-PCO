// Author(s) : Roosembert Palacios, Joan Maillard

#include "primenumberdetector.h"
#include <algorithm>
#include <cassert>

bool PrimeNumberDetector::isPrime(uint64_t number) {
  uint64_t maxCandidate = sqrtl(number);
  for (uint64_t candidate = 2; candidate < maxCandidate;
       candidate == 2 ? ++candidate : candidate += 2) {
    if (!(number % candidate))
      return false;
  }
  return true;
}

PrimeNumberDetectorMultiThread::PrimeNumberDetectorMultiThread(size_t nbThreads)
    : nbThreads(nbThreads) {}

typedef std::pair<uint64_t, uint64_t> interval;

/**
 * @brief Partitions the interval [to, from) in the specified number of slices.
 * The last interval may be smaller.
 */
static std::vector<interval> calculateChunks(uint64_t slices, uint64_t from,
                                             uint64_t to) {
  uint64_t size = to - from;
  uint64_t leftover = size % slices;
  if (leftover > 0) // Align the interval, to be removed later if needed.
    size += slices - leftover;

  uint64_t elems_per_slice = size / slices;
  std::vector<interval> intervals;
  for (uint64_t i = 0; i < slices; ++i) {
    uint64_t bottom = from + elems_per_slice * i;
    uint64_t top = from + elems_per_slice * (i + 1) - 1;
    intervals.push_back(std::make_pair(bottom, top));
  }
  if (leftover > 0) // Remove the interval alignment
    intervals.back().second -= slices - leftover;
  return intervals;
}

bool PrimeNumberDetectorMultiThread::isPrime(uint64_t number) {
  // Skip processing of all even numbers. (halves the function domain.)
  if (number % 2 == 0)
    return false;

  std::vector<interval> intervals =
      calculateChunks(nbThreads, 3, sqrtl(number) + 1);

  for (interval &interval : intervals) {
    if (interval.first % 2 == 0)
      interval.first++;
    if (interval.second % 2 == 0)
      interval.second--;
  }
  // => All the intervals start and end with an odd number

  bool result(true);
  std::vector<PcoThread *> threads;
  // Taking interval as a parameter to workaround weird bug where value-capture
  // Is deferred to lambda-instantiation time and the reference used as source
  // is no longer valid.
  // 'volatile' allows threads to signal one another early termination by
  // modifying the upper bound of the other thread's intervals.
  auto work = [number, &result, &intervals](volatile interval* interval) {
    for (uint64_t divisor = interval->first; divisor <= interval->second;
         divisor += 2) {
      if (number % divisor == 0) {
        // Induce termination of other threads.
        for (size_t i = 0; i < intervals.size(); ++i)
          intervals[i].second = 0;
        result = false; // Races are not a concern since they are convergent.
        return;
      }
    }
  };

  for (size_t i = 0; i < intervals.size(); ++i)
    threads.push_back(new PcoThread(work, &intervals[i]));

  for (size_t i = 0; i < threads.size(); ++i)
    threads[i]->join();

  for (size_t i = 0; i < threads.size(); ++i)
    delete threads[i];

  return result;
}
