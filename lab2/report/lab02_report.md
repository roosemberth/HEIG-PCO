---
toc: yes
date: 8 Mar 2021
title: Prime number detector
subtitle: PCO, Lab 02
author:
- Joan Maillard
- Roosembert Palacios
---

# Introduction

The premise of this lab work is to showcase two different solutions to a simple to imagine, yet complex to calculate problem:
The determination of whether a number is prime or not.
Two solutions are presented using a guided brute-force approach with one key difference:
One is multi-threaded, the other one is not.
This lab contains both implementations, in order to put into perspective the speed advantages of one over the other.

# Design decisions

## Implemented solution

The algorithm that we decided to use to detect prime numbers is as follows:

1. Determine whether the number is divisible by two.
1. Determine the square root of the input number.
1. Loop through the odd numbers between 3 and the square root of the input, both included:
   If any of these is an integral divisor of the input, return false, otherwise return true.

The single-threaded implementation sequentially tests every candidate divisor.

The multi-threaded implementation partitions the interval of candidates in as many subintervals as there are threads to be used.
If either thread finds an integral divisor, it induces termination in other threads and returns false.
If no search suceeds, true will be returned after all threads have finished.
It is important to note that there is an unchecked data race present in our code used for communicating whether the thread found a integral divisor;
however if such a race were to happen, the race's result will be convergent, therefore preventing it is unnecessary.

## Disgression on alternate solutions

We thought about implementing a separate solution that makes use of a cache made of pre-processed prime numbers in order to minimize the number of potential candidates.
It would have differed from the current implementation as follows:

1. Instead of considering 2 and every subsequent odd number as a candidate, the algorithm would first check through every entry in the cache, which is exclusively comprised of known prime numbers.
   If the square root of the input is reached with that list, the execution stops there and the returned result is true.
1. If the end of the cache is reached but the square root of the input is not, then the algorithm interrupts its execution for the current input and tries to find the next odd number that is a prime number, caches it, then considers it a candidate.
  Rinse and repeat as necessary.

It is important to note that given the fact that the amount of numbers contained within the scope of the lab is finite, it would have been possible to pre-calculate
the entirety of the cache and to ship it with the project in order to abide by the "constant complexity regardless of number of executions" premise of the lab[^1]

[^1]: This assessment has been established by a question asked to the teacher on _MS Teams_, live, March 5, 2021, regarding the implementation of this alternate solution.

One of the main downsides of this alternate solution is the increased space complexity.
Without providing a mechanism for eviction, 203280221 prime numbers are representable on a 32bit system amounting to 776MiB of static storage.

# Technical notes

- The calculated intervals are aligned to a multiple of the number of threads
  and then the last is shrunk if needed.
- The calculated intervals boundaries are clamped to odd numbers, this helps the
  processor streaming pipeline by avoiding conditionals upon processing which
  prevents stalling the instruction pipeline and promotes sucessfull speculative
  memory lookaheads.
- Signaling thread termination is made by reducing their search space instead.
  Since `PCOThread` does not provide primitives for abrupt thread termination
  (which in this case would be more appropiate), cooperative termination is
  required.
  Instead of checking for a shared flag (which would add at least one
  instruction and a conditional jump to each iteration) threads directly write
  to the memory of other threads.
  Doing so will flag the cache interconnect to force a cache miss, but this is
  not an issue since such event occurs only on thread termination and the
  performance penalty is thus acceptable.

# Benchmark results
## Preamble
Due to the instability of the virtual machine environment, it is important to note that the
benchmark program crashed every time it started running multi-threaded test, in spite of the
main and test programs running smoothly and without issues. It is therefore our conclusion that
our program is fully functional, and the environment is the problem.

## Benchmark data
Our benchmark data is reliant exclusively on runtime, which unfortunately is not as accurate a test
as measuring the number of iterations the program went through. However, since we were not able
to make any measurements with the benchmark program, we were forced to run a slightly modified version
of the test program. Therefore, our test data measures the execution time taken by the determination of
prime status over the same numbers.

All tests are run with 16 CPU threads allocated for the virtual machine.
Our test results are as follows:

Test type        Test duration
--------------- --------------
singlethreaded   1900ms
multi 1 thread   1909ms
multi 2 thread   989ms
multi 4 thread   507ms
multi 12thread   273ms
multi 16thread   266ms
multi 24thread   260ms
multi 32thread   257ms

## Benchmark conclusions
While our benchmark data isn't precise in and of itself, and couldn't be used to compare two different algorithms very accurately, it does show the lessening benefits of an increasing number of threads running the
same algorithm every time.

Indeed, the progression in runtime is not linear, but resembles a decreasing curve similar to that of the
function f(x) = 1/x, as expected due to the work repartition that our algorithm uses.

However, the simple fact of running a number of threads superior to 1 makes for a substantial improvement.
It is interesting to note that the runtime for multithreaded with 1 thread and singlethreaded is almost
equivalent, although the first is slightly slower than the latter because of the additional work that
its behavior imposes (e.g. the computation of the number of elements each thread will treat, as
opposed to simply running without that additional computation due to the single-threaded nature of
the latter implementation).

It is also interesting to note that once the threshold of number of parallel threads the computer is
capable of running due to the CPU that runs it is reached, the time gains are even more lessened.

# Conclusion

During the development of the exercise we found some issues on with the
`pco_synchro` lib, which were duely reported and thankfully corrected soon
enough upstream. Such corrections arrived after our workarounds were implemented
so they were not integrated into our final code.

An interesting extension to this lab would be to use AVX instructions and
perform multiple mod checks per iteration.
Another possible extension would be to implement a
[Sieve of Eratosthenes][sieve] by precalculating a [poset][poset] tensor and
apply a reduction by discrimination.
Though such approach would be better suited for GPU compute units, since they
employ a similar type of primops for scheduling fragment shaders.

[sieve]: https://en.wikipedia.org/wiki/Sieve_of_Eratosthenes
[poset]: https://en.wikipedia.org/wiki/Partially_ordered_set
