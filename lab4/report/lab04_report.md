---
toc: yes
date: 26 Apr 2021
title: Resource management
subtitle: PCO, Lab 04
author:
- Joan Maillard
- Roosembert Palacios
---

# Introduction {.nopagebreak}

The premise of this lab work is to allow us to grasp one of the main concepts 
of this class: managing _shared resources_ through the usage of previously seen
_semaphores_.

# Design decisions

We decided to work with maquet B.

After analysis of the problem, in order to implement the simulator in 
accordance with the functional constraints list provided in the lab description file, 
the following elements were deduced:
- Several critical sections have to be defined
- Given the fixed nature of the paths taken by either train, all of said critical sections needed to be predetermined.
- To address this problem ahead of time and to allow us to choose the path that would allow the most thorough testing phase possible, we decided to make every potential intersection a shared section.
- In order to implement that solution, it was necessary for us to define what different types of intersections existed in our maquet. These are represented in the synchro.h file. 
- In this case, they are a set of 3 different types:
    1. 4-ended, 2-shunt intersections, akin to 3, 4 red.
    1. 3-ended, 1-shunt intersections, akin to 2 red.
    1. 4-ended, 1-shunt intersections, akin to 17 red.
- The behavior for our locomotives was designed on the files locomotivebehavior.h and *.cpp.

# Technical notes
- Maquet B can be topologically modeled as an orientable manifold. This simplifies its management, allowing us to use [Kelvin-Stoke's criterion][1] to define right and left as aribrary groups of points to decide the direction of entry to a shared section. For more details, please consult code comments in file sectionmanager.h.
- As a consequence of how our project handles stopping behavior, certain situations make the maquet fail when stopping a locomotive at certain intersections, if inertia is enabled.

# Conclusion

In order to stay within the scope of the assignment, we had to drop a lot of our ideas in terms of segmentation of the problem, as well as its implementation.
In particular, we would have wished to make the entire board dynamically managed, and able to handle any number of locomotives, with paths that get defined over the course of the way in, stored, 
and then reused on the way back, with a decisional part of the program that decides each locomotive's first path on the fly. This paired with a much finer-grained approach to subdividing the board
would have made for a better solution in our opinion, but it happens to be outside of the scope of this laboratory, and its implementation would have extended development time by a consequent 
factor. 

[1]: https://en.wikipedia.org/wiki/Stokes%27_theorem