//    ___  _________    ___  ___  ___   __ //
//   / _ \/ ___/ __ \  |_  |/ _ \|_  | / / //
//  / ___/ /__/ /_/ / / __// // / __/ / /  //
// /_/   \___/\____/ /____/\___/____//_/   //
//                                         //
// Auteurs : Palacios Roosembert, Maillard Joan
//
#ifndef LOCOMOTIVEBEHAVIOR_H
#define LOCOMOTIVEBEHAVIOR_H

#include <deque>

#include "launchable.h"
#include "locomotive.h"
#include "sectionmanager.h"
#include "synchrointerface.h"

/**
 * @brief La classe LocomotiveBehavior représente le comportement d'une
 * locomotive
 */
class LocomotiveBehavior : public Launchable {
public:
  /*!
   * \brief locomotiveBehavior Constructeur de la classe
   * \param loco la locomotive dont on représente le comportement
   * \param sm Le gestionnaire de section partagées pour la maquette B
   */
  LocomotiveBehavior(Locomotive &loco,
                     const SharedSectionManagerForMaquetteB &sm)
      : loco(loco), sm(sm), directionAvant(true) {
    assignRoute(directionAvant);
    setStationContactId();
  };

  const static std::vector<int> route7;
  const static std::vector<int> route42;

protected:
  /*!
   * \brief run Fonction lancée par le thread, représente le comportement de la
   * locomotive
   */
  void run() override;

  /*!
   * \brief printStartMessage Message affiché lors du démarrage du thread
   */
  void printStartMessage() override;

  /*!
   * \brief printCompletionMessage Message affiché lorsque le thread a terminé
   */
  void printCompletionMessage() override;

  /**
   * @brief loco La locomotive dont on représente le comportement
   */
  Locomotive &loco;

  /**
   * @brief sm Section manager for Maquette B
   */
  const SharedSectionManagerForMaquetteB &sm;

private:
  /**
   * @brief lastContactIdx index du dernier contacte la loco a touché.
   */
  int lastContactIdx;

  /**
   * @brief nextContactIdx index du du contacte vers lequel on se dirige.
   */
  int nextContactIdx;

  /**
   * @brief directionAvant vraie si la locomotive se déplace vers l'avant.
   */
  bool directionAvant;

  /**
   * @brief route Route prévue pour la locomotive.
   */
  std::deque<int> route;

  /**
   * @brief Attribue un itineraire à suivre à la locomotive.
   *
   * L'itineraire est attribué en fonction du numéro de locomotive et la
   * direction de déplacement.
   *
   * @param sensAvant Si attribuer un itineraire où la locomotive se déplace
   *                  vers l'avant. Déplacement vers l'arrièrre le cas écheant.
   */
  void assignRoute(bool sensAvant);

  /**
   * @brief The shunt section we're currently going through, nullptr otherwise.
   */
  std::shared_ptr<SynchroInterface> shuntSection;

  /**
   * @brief contactId where the locomotive leaves the current shunt;
   *        zero otherwise.
   */
  int shuntExitContactId = 0;

  /**
   * @brief The shared section we're currently going through, nullptr otherwise.
   */
  std::shared_ptr<SynchroInterface> sharedSection;

  /**
   * @brief contactId where the locomotive leaves the current shared section;
   *        zero otherwise.
   */
  int sharedSectionExitContactId = 0;

  /**
   * @brief contactId of the station of this line.
   */
  int stationContactId = 0;

  /**
   * @brief sets the stationContactId corresponding to the station of this line.
   */
  void setStationContactId();
};

#endif // LOCOMOTIVEBEHAVIOR_H
