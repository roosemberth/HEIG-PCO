//    ___  _________    ___  ___  ___   __ //
//   / _ \/ ___/ __ \  |_  |/ _ \|_  | / / //
//  / ___/ /__/ /_/ / / __// // / __/ / /  //
// /_/   \___/\____/ /____/\___/____//_/   //
//                                         //
// Auteurs : Palacios Roosembert, Maillard Joan
//
#include "locomotivebehavior.h"
#include "ctrain_handler.h"

const std::vector<int> LocomotiveBehavior::route7({11, 6, 3, 19, 20, 8, 7, 15,
                                                   18, 23, 22, 1, 2, 7, 15, 18,
                                                   23});
const std::vector<int> LocomotiveBehavior::route42({12, 17, 18, 23, 22, 1, 25,
                                                    20, 19, 3, 4, 9, 10, 15, 16,
                                                    21, 22, 3, 6, 11});

void LocomotiveBehavior::run() {
  // Initialisation de la locomotive
  loco.allumerPhares();
  loco.demarrer();
  loco.afficherMessage("Ready!");

  while (1) {
    if (route.size() < 2) {
      loco.afficherMessage("Fin de ligne, inversion du trajet.");
      directionAvant = !directionAvant;
      assignRoute(directionAvant);
      lastContactIdx = route.front();
      route.pop_front();
      nextContactIdx = route.front();
      loco.inverserSens();
    }
    int shunt = sm.getShuntForPath(lastContactIdx, nextContactIdx);
    if (shunt != 0)
      diriger_aiguillage(abs(shunt), shunt > 0 ? TOUT_DROIT : DEVIE, 0);

    attendre_contact(nextContactIdx);
    if (nextContactIdx == shuntExitContactId) {
      shuntSection->leave(loco);
      shuntSection = nullptr;
      shuntExitContactId = 0;
    }
    if (nextContactIdx == sharedSectionExitContactId) {
      sharedSection->leave(loco);
      sharedSection = nullptr;
      sharedSectionExitContactId = 0;
    }

    if (nextContactIdx == stationContactId)
      sm.station->stopAtStation(loco);

    // Move ahead
    lastContactIdx = route.front();
    route.pop_front();
    nextContactIdx = route.front();

    shuntSection = sm.getShuntSection(lastContactIdx, nextContactIdx);
    if (shuntSection != nullptr) { // Approaching shunt, ensure we can continue
      shuntExitContactId = nextContactIdx;
      auto nextSection = sm.getSectionForPath(lastContactIdx, nextContactIdx);
      sharedSection = nextSection.first;
      sharedSectionExitContactId = nextSection.second;
      if (sharedSection == nullptr) {
        afficher_message(
            qPrintable(QString("La locomotive %1 n'a plus de chemin !")
                           .arg(loco.numero())));
      }
      sharedSection->access(loco);
      shuntSection->access(loco);
    }
  }
}

void LocomotiveBehavior::assignRoute(bool sensAvant) {
  int numLoco = loco.numero();
  const std::vector<int> *refRoute;
  switch (numLoco) {
  case 7:
    lastContactIdx = 11;
    nextContactIdx = 6;
    refRoute = &route7;
    break;
  case 42:
    lastContactIdx = 18;
    nextContactIdx = 23;
    refRoute = &route42;
    break;
  default:
    throw new std::runtime_error("Comportement inconnu pour la loco numéro " +
                                 std::to_string(numLoco));
  }
  if (sensAvant)
    route = std::deque<int>(refRoute->begin(), refRoute->end());
  else
    route = std::deque<int>(refRoute->rbegin(), refRoute->rend());
  // La locomotive est toujours après le premier point de contacte.
  lastContactIdx = route.front();
  route.pop_front();
  nextContactIdx = route.front();
}

void LocomotiveBehavior::printStartMessage() {
  qDebug() << "[START] Thread de la loco" << loco.numero() << "lancé";
  loco.afficherMessage("Je suis lancée !");
}

void LocomotiveBehavior::printCompletionMessage() {
  qDebug() << "[STOP] Thread de la loco" << loco.numero()
           << "a terminé correctement";
  loco.afficherMessage("J'ai terminé");
}

void LocomotiveBehavior::setStationContactId() {
  int numLoco = loco.numero();
  switch (numLoco) {
  case 7:
    stationContactId = 18;
    break;
  case 42:
    stationContactId = 16;
    break;
  default:
    throw new std::runtime_error("Comportement inconnu pour la loco numéro " +
                                 std::to_string(numLoco));
  }
}
