#ifndef SECTION_MANAGER_H
#define SECTION_MANAGER_H

#include <vector>

#include "synchro.h"
#include "synchrointerface.h"

/**
 * @brief Mediates entry to shunts and shared sections from contact points.
 *
 * Maquette B is hereby modeled as an orientable manifold.
 * We use Kelvin-Stoke's criterion to define right and left as arbitrary groups
 * of points to decide the direction of entry to a shunt or a shared section.
 */
class SharedSectionManagerForMaquetteB {
private:
  std::vector<std::shared_ptr<SynchroInterface>> shuntSections = {
      std::make_shared<Synchro>(), // Top left (4-3)
      std::make_shared<Synchro>(), // Top right (16-15)
      std::make_shared<Synchro>(), // Bottom right (11-12)
      std::make_shared<Synchro>(), // Bottom left (7-8)
      std::make_shared<Synchro>(), // Center up (2 & 14)
      std::make_shared<Synchro>(), // Center bottom (10 & 6)
      std::make_shared<Synchro>(), // Center cross-rail (17)
  };

  std::vector<std::shared_ptr<SynchroInterface>> sharedSections = {
      std::make_shared<Synchro>(), // Outer top
      std::make_shared<Synchro>(), // Outer right
      std::make_shared<Synchro>(), // Outer bottom
      std::make_shared<Synchro>(), // Outer left
      std::make_shared<Synchro>(), // Top left intermediate
      std::make_shared<Synchro>(), // Top right intermediate
      std::make_shared<Synchro>(), // Right intermediate
      std::make_shared<Synchro>(), // Bottom right intermediate
      std::make_shared<Synchro>(), // Bottom left intermediate
      std::make_shared<Synchro>(), // Left intermediate
      std::make_shared<Synchro>(), // Right upper inner circle
      std::make_shared<Synchro>(), // Right in-between inner circles
      std::make_shared<Synchro>(), // Right lower inner circle
      std::make_shared<Synchro>(), // Left lower inner circle
      std::make_shared<Synchro>(), // Left in-between inner circles
      std::make_shared<Synchro>(), // Left upper inner circle
      std::make_shared<Synchro>(), // Top-right center
      std::make_shared<Synchro>(), // Bottom-right center
      std::make_shared<Synchro>(), // Bottom-left center
      std::make_shared<Synchro>(), // Top-left center
  };

  /**
   * @brief A right-handed traversal of the contact points in the maquette.
   *
   * This path satisfies the property "all right-handed paths of lenght two are
   * a substring of this path". In particular, if a path of lenght two is not a
   * substring of this path, it is lefthanded.
   *
   * This should be calculated from the maquette data, but we were explicitly
   * instructed not to do generic calculations, hence the values were manually
   * calculated for the maquette B only.
   */
  const std::vector<int> rightHandPath = {
      // External ring
      5, 24, 23, 18, 17, 12, 11, 6,
      // Intermediate ring
      3, 22, 21, 16, 15, 10, 9, 4, 3,
      // Upper ring
      19, 20, 25, 1, 19, 14,
      // Lower ring
      13, 7, 8, 26, 13, 7, 2, 1,
      // Upper ring to intermediate
      22, 23, 16, 15, 10, 9, 4, 3, 19, 14,
      // Lower ring to intermediate
      13, 10, 9, 4, 3, 22, 21, 16, 15, 7, 2, 1, 22,
      // Intermediate to external
      23, 18, 15, 10, 11, 6, 3, 22, 21, 16, 17, 12, 9, 4, 3};

  /**
   * Returns the shunt section encountered by a right-handed traversal at the
   * specified contact point.
   *
   * @returns nullptr if no shunt section is at this point.
   */
  std::shared_ptr<SynchroInterface>
  getRhShuntSection(int entryContactId) const {
    switch (entryContactId) {
    // Top left
    case 6:
    case 4:
      return shuntSections[0];
    // Top right
    case 24:
    case 22:
      return shuntSections[1];
    // Bottom right
    case 18:
    case 16:
      return shuntSections[2];
    // Bottom left
    case 12:
    case 10:
      return shuntSections[3];
    // Center up
    case 1:
    case 3:
    case 25:
    case 19:
      return shuntSections[4];
    // Center bottom
    case 14:
    case 26:
    case 7:
      return shuntSections[5];
    // Center cross-rail
    case 8:
    case 20:
      return shuntSections[6];
    }
    return nullptr;
  }

  /**
   * Returns the shunt section encountered by a left-handed traversal at the
   * specified contact point.
   *
   * @returns nullptr if no shunt section is at this point.
   */
  std::shared_ptr<SynchroInterface>
  getLhShuntSection(int entryContactId) const {
    switch (entryContactId) {
    // Top left
    case 5:
    case 3:
      return shuntSections[0];
    // Top right
    case 23:
    case 21:
      return shuntSections[1];
    // Bottom right
    case 17:
    case 15:
      return shuntSections[2];
    // Bottom left
    case 11:
    case 9:
      return shuntSections[3];
    // Center up
    case 22:
    case 19:
    case 14:
    case 20:
      return shuntSections[4];
    // Center bottom
    case 13:
    case 2:
    case 8:
      return shuntSections[5];
    // Center cross-rail
    case 25:
    case 26:
      return shuntSections[6];
    }
    return nullptr;
  }

  /**
   * Returns the id of the exit shunt encountered by a right-handed traversal at
   * the specified contact point.
   *
   * @param exitContactId the contact where the locomotive wishes to leave at.
   *
   * @returns zero if the specified location has no shunt else a number
   *          representing the shunt.
   *          If the number is positive the shunt should be set to straight,
   *          if the number is negative the shunt should be deviated.
   */
  int getShuntForRhPath(int exitContactId) const {
    switch (exitContactId) {
    // External-Intermediate circuits
    case 5:
      return 3;
    case 3:
      return -3;
    case 23:
      return 15;
    case 21:
      return -15;
    case 17:
      return 11;
    case 15:
      return -11;
    case 11:
      return 7;
    case 9:
      return -7;
    // Intermediate-Internal circuits
    case 22:
      return 13;
    case 19:
      return -13;
    case 10:
      return 5;
    case 7:
      return -5;
    // Internal circuits
    case 2:
      return 6;
    case 8:
      return -6;
    case 14:
      return 14;
    case 20:
      return -14;
    }
    return 0;
  }

  /**
   * Returns the id of the exit shunt encountered by a left-handed traversal at
   * the specified contact point.
   *
   * @param exitContactId the contact where the locomotive wishes to leave at.
   *
   * @returns zero if the specified location has no shunt else a number
   *          representing the shunt.
   *          If the number is positive the shunt should be set to straight,
   *          if the number is negative the shunt should be deviated.
   */
  int getShuntForLhPath(int exitContactId) const {
    switch (exitContactId) {
    // External-Intermediate circuits
    case 6:
      return 4;
    case 4:
      return -4;
    case 12:
      return 8;
    case 10:
      return -8;
    case 18:
      return 12;
    case 16:
      return -12;
    case 24:
      return 16;
    case 22:
      return -16;
    // Intermediate-Internal circuits
    case 15:
      return 9;
    case 13:
      return -9;
    case 3:
      return 1;
    case 1:
      return -1;
    // Internal circuits
    case 14:
      return 10;
    case 26:
      return -10;
    case 2:
      return 2;
    case 25:
      return -2;
    }
    return 0;
  }

  /**
   * Returns the shared section encountered by a right-handed entry at the
   * specified contact point along with its exit point contactId.
   *
   * @returns nullptr if no section would be entered (e.g. contact is inside or
   *          at the end of a section).
   */
  std::pair<std::shared_ptr<SynchroInterface>, int>
  getRhSharedSection(int entryContactId) const {
    switch (entryContactId) {
      case 5: // Outer top
        return std::make_pair(sharedSections[0], 24);
      case 23: // Outer right
        return std::make_pair(sharedSections[1], 18);
      case 17: // Outer bottom
        return std::make_pair(sharedSections[2], 12);
      case 11: // Outer left
        return std::make_pair(sharedSections[3], 6);
      case 3: // Top left intermediate
        return std::make_pair(sharedSections[4], 3);
      case 22: // Top right intermediate
        return std::make_pair(sharedSections[5], 22);
      case 21: // Right intermediate
        return std::make_pair(sharedSections[6], 16);
      case 15: // Bottom right intermediate
        return std::make_pair(sharedSections[7], 15);
      case 10: // Bottom left intermediate
        return std::make_pair(sharedSections[8], 10);
      case 9: // Left intermediate
        return std::make_pair(sharedSections[9], 4);
      case 19: // Right upper inner circle
        return std::make_pair(sharedSections[10], 19);
      case 14: // Right in-between inner circles
        return std::make_pair(sharedSections[11], 14);
      case 13: // Right lower inner circle
        return std::make_pair(sharedSections[12], 13);
      case 7: // Left lower inner circle
        return std::make_pair(sharedSections[13], 7);
      case 2: // Left in-between inner circles
        return std::make_pair(sharedSections[14], 2);
      case 1: // Left upper inner circle
        return std::make_pair(sharedSections[15], 1);
      case 20: // Top-right center
        return std::make_pair(sharedSections[16], 20);
      case 26: // Bottom-right center
        return std::make_pair(sharedSections[17], 26);
      case 8: // Bottom-left center
        return std::make_pair(sharedSections[18], 8);
      case 25: // Top-left center
        return std::make_pair(sharedSections[19], 25);
    }
    return std::make_pair(nullptr, 0);
  }

  /**
   * Returns the shared section encountered by a left-handed entry at the
   * specified contact point along with its exit point contactId.
   *
   * @returns nullptr if no section would be entered (e.g. contact is inside or
   *          at the end of a section).
   */
  std::pair<std::shared_ptr<SynchroInterface>, int>
  getLhSharedSection(int entryContactId) const {
    switch (entryContactId) {
      case 24: // Outer top
        return std::make_pair(sharedSections[0], 5);
      case 18: // Outer right
        return std::make_pair(sharedSections[1], 23);
      case 12: // Outer bottom
        return std::make_pair(sharedSections[2], 17);
      case 6: // Outer left
        return std::make_pair(sharedSections[3], 11);
      case 3: // Top left intermediate
        return std::make_pair(sharedSections[4], 3);
      case 22: // Top right intermediate
        return std::make_pair(sharedSections[5], 22);
      case 16: // Right intermediate
        return std::make_pair(sharedSections[6], 21);
      case 15: // Bottom right intermediate
        return std::make_pair(sharedSections[7], 15);
      case 10: // Bottom left intermediate
        return std::make_pair(sharedSections[8], 10);
      case 4: // Left intermediate
        return std::make_pair(sharedSections[9], 9);
      case 19: // Right upper inner circle
        return std::make_pair(sharedSections[10], 19);
      case 14: // Right in-between inner circles
        return std::make_pair(sharedSections[11], 14);
      case 13: // Right lower inner circle
        return std::make_pair(sharedSections[12], 13);
      case 7: // Left lower inner circle
        return std::make_pair(sharedSections[13], 7);
      case 2: // Left in-between inner circles
        return std::make_pair(sharedSections[14], 2);
      case 1: // Left upper inner circle
        return std::make_pair(sharedSections[15], 1);
      case 20: // Top-right center
        return std::make_pair(sharedSections[16], 20);
      case 26: // Bottom-right center
        return std::make_pair(sharedSections[17], 26);
      case 8: // Bottom-left center
        return std::make_pair(sharedSections[18], 8);
      case 25: // Top-left center
        return std::make_pair(sharedSections[19], 25);
    }
    return std::make_pair(nullptr, 0);
  }

  /**
   * @brief Verifies whether the vector is a substring of the rightHandPath.
   */
  bool pathIsRightHanded(int fromContactIdx, int toContactIdx) const {
    for (auto e1p = rightHandPath.begin(); e1p != rightHandPath.end(); ++e1p) {
      if (*e1p != fromContactIdx)
        continue;
      auto e2p = e1p;
      ++e2p;
      if (*e2p == toContactIdx)
        return true;
    }
    return false;
  }

public:
  /**
   * @brief return the shunt section encountered from the specified vector.
   *
   * @returns the shunt section encountered when coming from and into the
   *          specified contacts or nullptr if there's no shunt section.
   */
  std::shared_ptr<SynchroInterface> getShuntSection(int fromContactIdx,
                                                    int toContactIdx) const {
    if (pathIsRightHanded(fromContactIdx, toContactIdx))
      return getRhShuntSection(fromContactIdx);
    return getLhShuntSection(fromContactIdx);
  }

  /**
   * Returns the id of the exit shunt encountered from the specified vector.
   *
   * @returns zero if the specified location has no shunt else a number
   *          representing the shunt.
   *          If the number is positive the shunt should be set to straight,
   *          if the number is negative the shunt should be deviated.
   */
  int getShuntForPath(int fromContactIdx, int toContactIdx) const {
    // The way the central shunt (17) is connected makes it so adjacent contacts
    // have more than one shunt for each given orientation (either lf or rh).
    // We therefore treat this shunt specially.
    if (fromContactIdx == 20) {
      if (toContactIdx == 25)
        return -17;
      else if (toContactIdx == 8)
        return 17;
    } else if (fromContactIdx == 26) {
      if (toContactIdx == 8)
        return -17;
      else if (toContactIdx == 25)
        return 17;
    } else if (fromContactIdx == 8) {
      if (toContactIdx == 26)
        return -17;
      else if (toContactIdx == 20) {
        return 17;
      }
    } else if (fromContactIdx == 25) {
      if (toContactIdx == 20)
        return -17;
      else if (toContactIdx == 26)
        return 17;
    }
    // Contact is not adjacent to central shunt, proceed.

    if (pathIsRightHanded(fromContactIdx, toContactIdx)) {
      return getShuntForRhPath(toContactIdx);
    }
    return getShuntForLhPath(toContactIdx);
  }

  /**
   * Returns the shared section from the specified vector along with its exit
   * point contactId. The toContactIdx should be the entry to the shared section.
   *
   * @returns zero if the specified location has no shunt else a number
   *          representing the shunt.
   *          If the number is positive the shunt should be set to straight,
   *          if the number is negative the shunt should be deviated.
   */
  std::pair<std::shared_ptr<SynchroInterface>, int>
  getSectionForPath(int fromContactIdx, int toContactIdx) const {
    if (pathIsRightHanded(fromContactIdx, toContactIdx)) {
      return getRhSharedSection(toContactIdx);
    }
    return getLhSharedSection(toContactIdx);
  }

  std::shared_ptr<Synchro> station = std::make_shared<Synchro>();
};

#endif // SECTION_MANAGER_H
