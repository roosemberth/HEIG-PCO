//    ___  _________    ___  ___  ___   __ //
//   / _ \/ ___/ __ \  |_  |/ _ \|_  | / / //
//  / ___/ /__/ /_/ / / __// // / __/ / /  //
// /_/   \___/\____/ /____/\___/____//_/   //
//                                         //
// Auteurs : Palacios Roosembert, Maillard Joan
//
#ifndef SYNCHRO_H
#define SYNCHRO_H

#include <QDebug>
#include <chrono>
#include <thread>

#include <pcosynchro/pcosemaphore.h>

#include "ctrain_handler.h"
#include "locomotive.h"
#include "synchrointerface.h"

/**
 * @brief La classe Synchro implémente l'interface SynchroInterface qui
 * propose les méthodes liées à la section partagée.
 */
class Synchro final : public SynchroInterface {
public:
  /**
   * @brief Synchro Constructeur de la classe qui représente la section
   * partagée. Initialisez vos éventuels attributs ici, sémaphores etc.
   */
  Synchro()
      : busy(false), busyMutex(1), nWaiting(0), waitingMutex(1),
        waitingQueue(0), waitingAtStation(false), stationMutex(1),
        stationQueue(0){};

  /**
   * @brief access Méthode a appeler pour indiquer que la locomotive désire
   * accéder à la section partagée (deux contacts avant la section partagée).
   * @param loco La locomotive qui désire accéder
   */
  void access(Locomotive &loco) override {
    busyMutex.acquire();
    if (busy) {
      busyMutex.release();
      loco.arreter();
      loco.eteindrePhares();
      waitingMutex.acquire();
      nWaiting++;
      waitingMutex.release();
      waitingQueue.acquire();
      loco.allumerPhares();
      loco.demarrer();
      return access(loco);
    }
    busy = true;
    busyMutex.release();

    afficher_message(qPrintable(
        QString("Engine no. %1 entering shared section.").arg(loco.numero())));
  }

  /**
   * @brief stopAtStation Méthode à appeler pour la section partagée de type
   * gare et gérer la correspondance, doit arrêter la locomotive et mettre son
   * thread en attente si la deuxieme locomotive est absente ou démarrer la
   * correspondance si cette derniere est déjà présente.
   * @param loco La locomotive qui arrive en gare
   */
  void stopAtStation(Locomotive &loco) override {
    bool shouldQueue = false;
    stationMutex.acquire();
    if (waitingAtStation) {
        waitingAtStation = false;
        stationQueue.release();
    } else {
        waitingAtStation = true;
        shouldQueue = true;
    }
    stationMutex.release();

    loco.arreter();
    loco.eteindrePhares();
    if (shouldQueue) {
        stationQueue.acquire();
    }
    std::chrono::seconds waitDuration(5);
    std::this_thread::sleep_for(waitDuration);
    loco.allumerPhares();
    loco.demarrer();
  }

  /**
   * @brief leave Méthode à appeler pour indiquer que la locomotive est sortie
   * de la section partagée. (reveille les threads des locomotives
   * potentiellement en attente).
   * @param loco La locomotive qui quitte la section partagée
   */
  void leave(Locomotive &loco) override {
    bool notifyQueue = false;
    busyMutex.acquire();
    busy = false;
    waitingMutex.acquire();
    if (nWaiting > 0) {
      notifyQueue = true;
      nWaiting--;
    }
    waitingMutex.release();
    busyMutex.release();
    if (notifyQueue)
      waitingQueue.release();
  }

private:
  /**
   * @brief Si la section partagée est actuellement ocupée.
   */
  bool busy;
  PcoSemaphore busyMutex;

  /**
   * @brief number of locomotives waiting to access this shared section.
   */
  int nWaiting;
  PcoSemaphore waitingMutex;

  /**
   * @brief semaphore where incoming locomotives will stall.
   */
  PcoSemaphore waitingQueue;

  /**
   * @brief whether another train is waiting at the station.
   */
  bool waitingAtStation = false;
  PcoSemaphore stationMutex;

  /**
   * @brief semaphore where trains at the train station will stall.
   */
  PcoSemaphore stationQueue;
};

#endif // SYNCHRO_H
