//    ___  _________    ___  ___  ___   __ //
//   / _ \/ ___/ __ \  |_  |/ _ \|_  | / / //
//  / ___/ /__/ /_/ / / __// // / __/ / /  //
// /_/   \___/\____/ /____/\___/____//_/   //
//                                         //
// Auteurs : Rick Wertenbroek & Bilal Melehi
// Rien à modifier ici

#ifndef SYNCHROINTERFACE_H
#define SYNCHROINTERFACE_H

#include "locomotive.h"

/**
 * @brief La classe SharedSectionInterface est une interface (classe abstraite pure) qui définit
 * trois méthodes, access, stopAtStation et leave afin de gérer l'accès à une section partagée d'un
 * parcours de locomotives.
 */
class SynchroInterface
{

public:

    /**
     * @brief access Méthode a appeler pour indiquer que la locomotive désire accéder à la
     * section partagée (deux contacts avant la section partagée).
     * @param loco La locomotive qui désire accéder
     */
    virtual void access(Locomotive& loco) = 0;

    /**
     * @brief stopAtStation Méthode à appeler pour la section partagée de type gare et gérer la correspondance,
     * doit arrêter la locomotive et mettre son thread en attente si la deuxieme locomotive est absente ou démarrer
     * la correspondance si cette derniere est déjà présente.
     * @param loco La locomotive qui arrive en gare
     */
    virtual void stopAtStation(Locomotive& loco) = 0;

    /**
     * @brief leave Méthode à appeler pour indiquer que la locomotive est sortie de la section
     * partagée. (reveille les threads des locomotives potentiellement en attente).
     * @param loco La locomotive qui quitte la section partagée
     */
    virtual void leave(Locomotive& loco) = 0;
};

#endif // SYNCHROINTERFACE_H
