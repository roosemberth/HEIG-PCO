//    ___  _________    ___  ___  ___   __ //
//   / _ \/ ___/ __ \  |_  |/ _ \|_  | / / //
//  / ___/ /__/ /_/ / / __// // / __/ / /  //
// /_/   \___/\____/ /____/\___/____//_/   //
//                                         //
// Auteurs : Palacios Roosembert, Maillard Joan
//
#include "ctrain_handler.h"

#include "locomotive.h"
#include "locomotivebehavior.h"
#include "sectionmanager.h"

static Locomotive locoA(7, 10);
static Locomotive locoB(42, 12);

void emergency_stop() {
  // TODO

  afficher_message("\nSTOP!");
}

int cmain() {
  selection_maquette(MAQUETTE_B);

  /**********************************
   * Initialisation des aiguillages *
   **********************************/

  diriger_aiguillage(16, TOUT_DROIT, 0);
  diriger_aiguillage(15, DEVIE, 0);
  diriger_aiguillage(12, DEVIE, 0);
  diriger_aiguillage(11, DEVIE, 0);
  diriger_aiguillage(9, TOUT_DROIT, 0);
  diriger_aiguillage(5, TOUT_DROIT, 0);
  diriger_aiguillage(8, DEVIE, 0);
  diriger_aiguillage(7, TOUT_DROIT, 0);
  diriger_aiguillage(4, TOUT_DROIT, 0);
  diriger_aiguillage(3, TOUT_DROIT, 0);

  /********************************
   * Position de départ des locos *
   ********************************/

  locoA.fixerPosition(6, 11);
  locoB.fixerPosition(17, 12);

  /***********
   * Message *
   **********/

  afficher_message("Hit play to start the simulation...");

  /*********************
   * Threads des locos *
   ********************/

  SharedSectionManagerForMaquetteB sm;

  // Création du thread pour la loco 0
  std::unique_ptr<Launchable> locoBehaveA =
      std::make_unique<LocomotiveBehavior>(locoA, sm);
  // Création du thread pour la loco 1
  std::unique_ptr<Launchable> locoBehaveB =
      std::make_unique<LocomotiveBehavior>(locoB, sm);

  // Lanchement des threads
  afficher_message(qPrintable(
      QString("Lancement thread loco A (numéro %1)").arg(locoA.numero())));
  locoBehaveA->startThread();
  afficher_message(qPrintable(
      QString("Lancement thread loco B (numéro %1)").arg(locoB.numero())));
  locoBehaveB->startThread();

  // Attente sur la fin des threads
  locoBehaveA->join();
  locoBehaveB->join();

  // Fin de la simulation
  mettre_maquette_hors_service();

  return EXIT_SUCCESS;
}
