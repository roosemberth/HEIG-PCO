---
title: Exercise 6.1
subtitle: PCO 2021
author: Roosembert Palacios
date: 22 Mars 2021
geometry: margin=2cm
---

Soit le graphe d'exécution de tâches suivant :

![Graphe d'exécution de tâches](img/thread-deps-graph.png){#id .class width=100}.

Écrire un programme permettant de garantir ce fonctionnement, en utilisant le fonction `PcoThread::join()`.

```c
void main() {
  PcoThread t1(...);

  t1.join();
  PcoThread t2(...);
  PcoThread t3(...);

  t2.join();
  PcoThread t4(...);
  PcoThread t5(...);
  PcoThread t6(...);

  t4.join();
  t5.join();
  t6.join();
  PcoThread t7(...);

  t7.join();
  t3.join();
  PcoThread t8(...);

  t8.join();
}
```
