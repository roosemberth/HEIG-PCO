# This is a Nix Flake. This file describes the necessary environment needed to
# build the labs. Nix is not required to build any of them, as long as the
# necessary dependencies are available in the system.
# This file is provided merely for convenience.
# See https://nixos.wiki/wiki/Flakes for more information about Nix flakes.
{
  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixos-20.09";

  outputs = { self, nixpkgs }: let
    systems = [ "x86_64-linux" "i686-linux" ];
    forAllSystems = f: nixpkgs.lib.genAttrs systems (system: f (import nixpkgs {
      inherit system; overlays = [ self.overlay ];
    }));
  in {
    overlay = final: prev: with final; let
      packageForLab = name: src: lab-env.overrideAttrs(_:{
          inherit name src;
          installPhase = ''
            mkdir -p "$out/bin"
            ${pkgs.findutils}/bin/find . -executable -type f \
              -exec cp '{}' "$out/bin/" \;
          '';
        });
    in {
      pco-synchro = stdenv.mkDerivation {
        pname = "pco-synchro";
        version = "0.0.0";
        src = fetchFromGitLab {
          owner = "reds-public";
          repo = "pco-synchro";
          rev = "cd9c88043692ab4eb22330ada31bece9f4a5abff";
          hash = "sha256-UCqLA7ED3zzgCmlAOXARtijbnQUZM/M1Pvc6HBDVJgk=";
        };
        nativeBuildInputs = [ qt5.qmake ];
        postPatch = ''
          substituteInPlace lib/pcosynchro/pcosynchro.pro \
            --replace '/usr/local/' "$out/"
        '';
        preConfigure = "cd lib/pcosynchro";
      };
      lab-env = stdenv.mkDerivation {
        name = "PCO-lab-env";
        nativeBuildInputs = [ qt5.qmake gtest gbenchmark ];
        buildInputs = [ pco-synchro qt5.wrapQtAppsHook ];
        allowSubstitutes = false;
      };
      lab1 = packageForLab "lab1" ./lab1/code;
      lab2 = packageForLab "lab2" ./lab2/code;
      lab3 = packageForLab "lab3" ./lab3/code;
      lab4 = lab-env.overrideAttrs(o:{
        name = "lab4";
        src = ./lab4;
        preConfigure = "cd code";
        buildInputs = o.buildInputs ++ [ qt5.qtmultimedia ];
        installPhase = ''
            mkdir -p "$out/bin"
            ${pkgs.findutils}/bin/find . -executable -type f \
              -exec cp '{}' "$out/bin/" \;
            cp -r ../QtrainSim/data "$out/bin"
        '';
      });
      lab6 = packageForLab "lab6" ./lab6/code;
    };

    devShell = forAllSystems (pkgs: with pkgs; lab-env.overrideAttrs(o: {
      nativeBuildInputs = o.nativeBuildInputs ++ [qtcreator];
    }));

    # Export all packages from the overlay attribute.
    packages = forAllSystems (pkgs: with pkgs.lib;
      getAttrs (attrNames (self.overlay {} {})) pkgs
    );
  };
}
