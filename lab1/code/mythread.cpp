#include <atomic>
#include <thread>
#include "mythread.h"

static volatile long unsigned int counter;
static std::atomic_flag spinlock(false);

void runTask(unsigned long nbIterations)
{
    long unsigned int i = 0;

    while (i < nbIterations)
    {
        /*
         * To lock... or not to lock...
         *
         * std::atomic_flag is the only specialization of std::atomic guaranteed
         * to be lock-free. In the case of the clang compiler [^1], such lock
         * would be deferred by the rts to the `_umtx_op` syscall [^2] [^3]:
         *
         * From _umtx_up(2):
         *   When a locking request cannot be immediately satisfied, the thread
         *   is typically put to sleep, which is a non-runnable state terminated
         *   by the wake operation.
         *
         * While scheduling is usually efficient, moving a thread in and out
         * scheduler queues is generally not [citation needed].
         *
         * Since we expect the critical section to be completed pretty fast, it
         * could be argued that the overhead per-thread of the sleep/wakeup in
         * the scheduler queues is bigger than 'k' context switchs with 4
         * instructions (see baz@[^1]); where k is the expected number of
         * context switchs until this thread can lock the variable (hence we
         * keep this thread 'spinning').
         *
         * With my current knowledge I do not know of a reliable way to study
         * this and I feel like the time learning about benchmarking methods
         * would not be justified in the scope of the allocated time for this
         * assignment.
         *
         * [^1]: https://gcc.godbolt.org/z/W8qbff
         * [^2]: https://github.com/llvm-mirror/compiler-rt/blob/master/lib/builtins/atomic.c#L181
         * [^3]: https://github.com/llvm-mirror/compiler-rt/blob/master/lib/builtins/atomic.c#L64
         */
        while (spinlock.test_and_set(std::memory_order_acquire))
          std::this_thread::yield();
        counter++;
        spinlock.clear(std::memory_order_release);
        i++;
    }
}

void initCounter()
{
    counter = 0;
}

long unsigned int getCounter()
{
    return counter;
}
